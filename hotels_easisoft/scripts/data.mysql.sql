-- scripts/data.sqlite.sql
--
-- You can begin populating the database with the following SQL statements.


-- old sqlite code
/*
INSERT INTO guestbook (email, comment, created) VALUES
    ('ralph.schindler@zend.com',
    'Hello! Hope you enjoy this sample zf application!',
    DATETIME('NOW'));
INSERT INTO guestbook (email, comment, created) VALUES
    ('foo@bar.com',
    'Baz baz baz, baz baz Baz baz baz - baz baz baz.',
    DATETIME('NOW'));
*/

-- MySQL code from quickStart Tutorial
-- using THE CHARACTER ` for the names of entries in this file is critical and without it won't work!!
/*
INSERT INTO `guestbook` 
	(`id`, `email`, `comment`, `created`) 
VALUES
	(1, 'ralph.schindler@zend.com', 'Hello! Hope you enjoy this sample zf application!', '2012-03-22 00:00:00'),
	(2, 'foo@bar.com', 'Baz baz baz, baz baz Baz baz baz - baz baz baz.', '2012-03-20 00:00:00');
*/

-- easisoft project
/*
INSERT INTO `hotels` (`name`, `address`, `stars`) 
VALUES
	('hilton', 'Bolzano via macello N.1', '5'),
	('fortina', 'Bolzano via museo N.2', '4'),
	('corinthia', 'Bolzano pza walter N.3', '5'),
	('calypso', 'Renon via galvani N.25', '4'),
	('cavalieri', 'Bolzano viale doruso N.74', '3'),
	('bella vista', 'Merano via cordiali N.36', '2'),
	('astra', 'Merano via castro N.44', '5'),
	('bayview', 'Caldaro via diego N.63', '2'),
	('cerviola', 'Merano via maria N.12', '3'),
	('alborada', 'Merano pza redica N.87', '3');
*/
INSERT INTO `pricelist` (`desc`, `price`, `hotel_id`) 
VALUES
	('single_room', 123.30, 1),
	('double_room', 211.70, 1),
	('family_room', 350, 1),
	('single_room', 110, 2),
	('double_room', 170, 2),
	('family_room', 250, 2),
	('single_room', 139, 3),
	('double_room', 249, 3),
	('family_room', 400, 3),
	('single_room', 99.99, 4),
	('double_room', 150, 4),
	('family_room', 210, 4),
	('single_room', 70.40, 5),
	('double_room', 116.50, 5),
	('family_room', 200, 5),
	('single_room', 40, 6),
	('double_room', 70, 6),
	('family_room', 110, 6),
	('single_room', 150, 7),
	('double_room', 220, 7),
	('family_room', 350, 7),
	('single_room', 50.20, 8),
	('double_room', 90.40, 8),
	('family_room', 126.80, 8),
	('single_room', 70, 9),
	('double_room', 155, 9),
	('family_room', 298, 9),
	('single_room', 65, 10),
	('double_room', 137.70, 10),
	('family_room', 185, 10);	
/*
INSERT INTO `users` (`username`, `password`, `email`, `role`) 
VALUES
	('root1', '123', 'root1@tester.com', 'root'),
	('root2', '321', 'root2@tester.com', 'root'),
	('admin1', '1234', 'admin1@tester.com', 'administrator'),
	('admin2', '1234', 'admin2@tester.com', 'administrator'),
	('regular1', '12345', 'regular1@tester.com', 'regular'),
	('regular2', '12345', 'regular2@tester.com', 'regular'),
	('regular3', '12345', 'regular3@tester.com', 'regular');

INSERT INTO `usershotels` (`user_id`, `hotel_id`) 
VALUES
	(1, 1),(1, 2),(1, 3),(1, 4),(1, 5),(1, 6),(1, 7),(1, 8),(1, 9),(1, 10),
	(2, 1),(2, 2),(2, 3),(2, 4),(2, 5),(2, 6),(2, 7),(2, 8),(2, 9),(2, 10),
	(3, 3),(3, 5),(3, 7),(3, 9),
	(4, 1), (4, 4),
	(5, 3),(5, 5),(5, 7),(5, 9),
	(6, 1),(6, 2),(6, 4),(6, 10),
	(7, 2),(7, 5),(7, 6),(7, 7), (7, 8),(7, 10);
	
