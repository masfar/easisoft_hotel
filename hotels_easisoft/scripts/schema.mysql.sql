-- scripts/schema.sqlite.sql
--
-- You will need load your database schema with this SQL.

-- old sqlite code
/*
CREATE TABLE guestbook (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    email VARCHAR(32) NOT NULL DEFAULT 'noemail@test.com',
    comment TEXT NULL,
    created DATETIME NOT NULL
);
CREATE INDEX "id" ON "guestbook" ("id");
*/


-- MySQL code from quickStart Tutorial
/*
CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(32) NOT NULL DEFAULT 'noemail@test.com',
  `comment` text,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;
*/

-- easisoft project
/*


*/

CREATE TABLE IF NOT EXISTS `usershotels` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`hotel_id` int(11) NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES users(`id`),
	FOREIGN KEY (`hotel_id`) REFERENCES hotels(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT = 1 ;

CREATE TABLE IF NOT EXISTS `pricelist` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`hotel_id` int(11) NOT NULL,
	`desc` varchar (32) NOT NULL,
	`price` double NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`hotel_id`) REFERENCES hotels(`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT = 1 ;

CREATE TABLE IF NOT EXISTS `hotels` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(32) NOT NULL DEFAULT 'noNamed',
	`address` text,
	`stars` text,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT = 1 ;

CREATE TABLE IF NOT EXISTS `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(32) NOT NULL UNIQUE,
	`password` char(32) NOT NULL,
	`email` varchar(80) NULL,	
--    `active` BOOLEAN NOT NULL DEFAULT 1,
--    `salt` CHAR(20) NOT NULL,
    `role` varchar(32) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT = 1 ;
