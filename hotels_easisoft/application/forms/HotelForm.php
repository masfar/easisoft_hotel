<?php

class Application_Form_HotelForm extends Zend_Form
{

    public function init() {
        
        $this->setMethod('post');
        
        $this->addElement(
            'hidden', 
            'id', 
            array(
            'value' => null 
            )
        );
        
        $this->addElement(
            'text',
            'name',
            array(
                'label' => 'Name:',
                'required' => true,
                'value' => 'please enter hotel name',
                'filters' => array('StringTrim') 
            )
        );
        
        $this->addElement(
            'text',
            'address',
            array(
                'label' => 'Address:',
                'value' => 'please enter address',
                'required' => true,
                'filters' => array('StringTrim') 
            )
        );
        
        $this->addElement(
            'radio',
            'stars',
            array(
                'label' => 'Stars:',
                'value' => '1',
                'required' => true,
                'multiOptions' => array(
                                    '1' => '1',
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',
                                    '5' => '5'
                                    )
            )
        );
      
        $this->addElement(
            'Select', 
            'pricelist', 
            array(
                'label' => 'Price list:',
                'value' => 1,
                'size' => 5,
                'multiOptions' => array()
            )
        );
        
        $this->addElement(
            'submit',
            'price_edit',
            array(
                'label' => 'Edit',
                'ignore' => true
            )
        );
        
        $this->addElement(
            'submit',
            'price_new',
            array(
                'label' => 'New',
                'ignore' => true
            )
        );
        
        $this->addElement(
            'submit',
            'price_delete',
            array(
                'label' => 'Delete',
                'ignore' => true
            )
        );
        
        $this->addElement(
            'text',
            'desc_field',
            array(
                'label' => 'Desc:',
                'value' => 'no_room',
                'filters' => array('StringTrim') 
            )
        );
        
        $this->addElement(
            'text',
            'price_field',
            array(
                'label' => 'Price:',
                'value' => '0',
                'filters' => array('StringTrim') 
            )
        );
        
        $this->addElement(
            'submit',
            'price_save',
            array(
                'label' => 'Save',
                'ignore' => true
            )
        ); 
        
        $priceGroup = $this->addDisplayGroup(
            array( 'pricelist', 'price_delete', 'price_edit', 'price_new' ),
            'pricegroup'
        );
        
        $this->addDisplayGroup(
            array( 'desc_field', 'price_field', 'price_save' ),
            'priceItemgroup'
        );
    
        $this->addElement(
            'submit',
            'save',
            array(
                'label' => 'Save',
                'ignore' => true
            )
        );
        
        $this->addElement(
            'submit',
            'delete',
            array(
                'label' => 'Delete',
                'ignore' => true
            )
        );
    }

    public function populate(array $data) {
        
        foreach($data as $field => $value) {
            
            if ($field == 'pricelist') $this->{$field}->setMultiOptions($value);
            else $this->{$field}->setValue($value);
        }
        return $this;
    }
}

