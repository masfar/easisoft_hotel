<?php

class Application_Form_UserForm extends Zend_Form
{

public function init() {
        
        $this->setMethod('post');
        
        $this->addElement('hidden', 'id', array(
            'value'      => null
        ));
        
        $this->addElement(
            'text',
            'username',
            array(
                'label' => 'Username:',
                'required' => true,
                'value' => 'please enter user name',
                'filters' => array('StringTrim') )
        );
        
         $this->addElement(
            'password',
            'password',
            array(
                'label' => 'Password:',
                'value' => 'please enter password',
                'required' => true,
                'filters' => array('StringTrim') )
        );
         
         $this->addElement(
             'text',
             'email',
             array(
                 'label' => 'Email:',
                 'required' => true,
                 'value' => 'please enter Email',
                 'filters' => array('StringTrim') )
         );
        
        $this->addElement(
            'select',
            'role',
            array(
                'label' => 'Role:',
                'value' => 'regular',
                'required' => true,
        'multiOptions'=>array(
            'regular' => 'regular',
            'admin' => 'admin',
            'root' => 'root'
        )));
      
        $this->addElement('Select', 'hotels', array(
            'label' => 'Hotels:',
            'value' => 1,
            'size' => 5,
            'multiOptions' => array(
        )
        ));
                
        $this->addElement(
            'button',
            'edit',
            array(
                'label' => 'Edit',
                'ignore' => true
            )
        );
        $this->addDisplayGroup(
            array('hotels', 'edit'),
            'hotelgroup'
            
        );
        
        $this->addElement(
            'submit',
            'save',
            array(
                'label' => 'Save',
                'ignore' => true
            )
        );
        
        $this->addElement(
            'submit',
            'delete',
            array(
                'label' => 'Delete',
                'ignore' => true
            )
        );
    }

    public function populate(array $data) {
        
        foreach($data as $field => $value) {
            
            if ($field == 'hotels') $this->{$field}->setMultiOptions($value);
            else $this->{$field}->setValue($value);
        }
        return $this;
    }
}