<?php

class EditUserController extends Zend_Controller_Action
{

    public function init()
    {
       
    }

    public function indexAction()
    {
        $userMapper = new Application_Model_UserMapper();
        $hotelMapper = new Application_Model_HotelMapper();
        $userForm = new Application_Form_UserForm();
        
        //retrieves the user ID who signed-in
        $username = Zend_Auth::getInstance()->getIdentity();
        $id = $this->getRequest()->getParam('id');
        $user= $userMapper->findUserById($id);
        
       
//         $pricelist = array();
        
//         foreach ($user->getPriceList() as $item){
//             $pricelist[$item->getId()]= $item->getDesc() . " - Euro " . $item->getPrice();
//         }
        $userForm->populate(array(
            'id' => $user->getId(),
            'username' => $user->getUserName(),
            'password' => $user->getPassword(),
            'email' => $user->getEmail(),
            'role' => $user->getRole(),
            //'pricelist' => $pricelist
        ));
        var_dump($user->getRole());
        $this->view->userForm = $userForm;
    }

    public function newAction()
    {
        $userMapper = new Application_Model_UserMapper();
        $userForm = new Application_Form_UserForm();
        $user = new Application_Model_User($userForm->getValues());
        $userMapper->save($user);
        $this->view->userForm = $userForm;
    }

    public function saveAction()
    {
        if(isset($_POST['delete'])){
            $this->deleteAction();
            return;
        }
        
        $request = $this->getRequest();
        $form = new Application_Form_UserForm();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                $user = new Application_Model_User($form->getValues());
                $mapper = new Application_Model_UserMapper();
                $mapper->save($user);
                
                //return $this->_helper->redirector('/hotels');
            }
            else echo "Invalid Form submited, something missing?";
        }
        
        $this->view->form = $form;
    }

    public function deleteAction(){
        $mapper = new Application_Model_UserMapper();
        $id = $this->getRequest()->getParam('id');
        $mapper->delete($id);
        
        
    }

}





