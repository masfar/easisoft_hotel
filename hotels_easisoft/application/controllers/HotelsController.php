<?php

class HotelsController extends Zend_Controller_Action
{

    
    public function init()
    {
        
    }
    
    
    public function indexAction()
    {
        $userMapper = new Application_Model_UserMapper();
        $hotelMapper = new Application_Model_HotelMapper();
        //retrieves the user ID who signed-in
        $username = Zend_Auth::getInstance()->getIdentity();
        $user = $userMapper->findUserBy($username);
        $userId = $user->getId();
        if ($user->getRole() === "root")
            $this->view->entries = $hotelMapper->fetchAll();
        else             
            $this->view->entries = $userMapper->findHotelsByUser($userId);
        
        //$loginForm = new Application_Form_Login();
        //$adapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');
        //var_dump($username);
    }
}

