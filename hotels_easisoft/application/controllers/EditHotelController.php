<?php

class EditHotelController extends Zend_Controller_Action
{

    
    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $userMapper = new Application_Model_UserMapper();
        $hotelMapper = new Application_Model_HotelMapper();
        $hotelForm = new Application_Form_HotelForm();
        
        //retrieves the user ID who signed-in
        $username = Zend_Auth::getInstance()->getIdentity();
        $userId = $userMapper->findUserBy($username)->getId();
        
        $hotelId = $this->getRequest()->getParam('id');
        $priceItemId = $this->getRequest()->getParam('priceItemId');
        $hotelData = $hotelMapper->findHotel($hotelId);
        $pricelist = array();
        $desc="";
        $price="";

        foreach ($hotelData->getPriceList() as $item) {
            $pricelist[$item->getId()]= $item->getDesc() . " - Euro " . $item->getPrice();
            
            if($priceItemId == $item->getId()) {
                $desc= $item->getDesc();
                $price= $item->getPrice();
            } 
        }
        
        $hotelForm->populate(array(
            'id' => $hotelData->getId(),
            'name' => $hotelData->getName(),
            'address' => $hotelData->getAddress(),
            'stars' => $hotelData->getStars(),
            'pricelist' => $pricelist,
            'desc_field' =>  $desc,
            'price_field' =>  $price,
        ));
        
        $this->view->hotelForm = $hotelForm;
    }

    public function saveAction() {
        
        if(isset($_POST['delete'])) {
            $this->deleteAction();
            return;
        }
        
        elseif (isset($_POST['price_new'])) {
            echo "NEW";
            $userMapper = new Application_Model_UserMapper();
            $hotelMapper = new Application_Model_HotelMapper();
            $pricelistMapper = new Application_Model_PriceListMapper();

            $request = $this->getRequest();
            $id = $_POST['id'];
            
            $pricelistMapper->add($id, "no_room", 0);
            return $this->redirect('/edit-hotel?id='.$id);
        
        } elseif (isset($_POST['price_delete'])) {
            echo "DELETE";
            $userMapper = new Application_Model_UserMapper();
            $hotelMapper = new Application_Model_HotelMapper();
            $pricelistMapper = new Application_Model_PriceListMapper();
            
            $request = $this->getRequest();
            $delIndex = $_POST['pricelist'];
            $id=$_POST['id'];
            $hotelData = $hotelMapper->findHotel($id);
            
            //$priceToDel=$hotelData->getPriceList()[];
            //$idToDelete=$priceToDel->getId();
            
            $pricelistMapper->delete($delIndex);
            return $this->redirect('/edit-hotel?id='.$id);
            
        } elseif (isset($_POST['price_edit'])) {
            echo "EDIT";
            $userMapper = new Application_Model_UserMapper();
            $hotelMapper = new Application_Model_HotelMapper();
            $pricelistMapper = new Application_Model_PriceListMapper();
            
            $request = $this->getRequest();
            $delIndex = $_POST['pricelist'];
            $id = $_POST['id'];
            $hotelData = $hotelMapper->findHotel($id);
            //$priceToDel=$hotelData->getPriceList()[];
            //$idToDelete=$priceToDel->getId();
            //$pricelistMapper->delete($delIndex);
            return $this->redirect('/edit-hotel?id='.$id."&priceItemId=".$delIndex);
            
        } elseif (isset($_POST['price_save'])) {
            echo "SAVE";
            $userMapper = new Application_Model_UserMapper();
            $hotelMapper = new Application_Model_HotelMapper();
            $pricelistMapper = new Application_Model_PriceListMapper();
            
            $request = $this->getRequest();
            $delIndex = $_POST['pricelist'];
            $id = $_POST['id'];
            
            $pricelistMapper->update($delIndex, $_POST['desc_field'], $_POST['price_field']);
            return $this->redirect('/edit-hotel?id='.$id."&priceItemId=".$delIndex);
        }
        
        $request = $this->getRequest();
        
        $form = new Application_Form_HotelForm();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                
                $hotel = new Application_Model_Hotel($form->getValues());
                $mapper = new Application_Model_HotelMapper();
                $mapper->save($hotel);
                //return $this->_helper->redirector('/hotels');
            }
            else echo "Invalid Form submited, something missing?";
        }
        
        $this->view->form = $form;
    }

    public function newAction() {
        
        $hotelMapper = new Application_Model_HotelMapper();
        $hotelForm = new Application_Form_HotelForm();
        $hotel = new Application_Model_Hotel($hotelForm->getValues());
        $hotelMapper->save($hotel);
        $this->view->hotelForm = $hotelForm;
    }
    
    public function deleteAction() {
        $hotelMapper = new Application_Model_HotelMapper();
        $hotelId = $this->getRequest()->getParam('id');
        $hotelMapper->delete($hotelId);
    }
}







