<?php

class UsersController extends Zend_Controller_Action
{

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        
        $userMapper = new Application_Model_UserMapper();
        $hotelMapper = new Application_Model_HotelMapper();
        //retrieves the user ID who signed-in
        $username = Zend_Auth::getInstance()->getIdentity();
        $user = $userMapper->findUserBy($username);
        $userId = $user->getId();
        
        if ($user->getRole() === "root")
            $this->view->entries = $userMapper->fetchAll();
        else {
            echo "Not authorized!";
            $this->view->entries = array();
        }             
    }
}

