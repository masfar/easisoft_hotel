<?php

class ChangeHotelOwnershipController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $userMapper = new Application_Model_UserMapper();
        $hotelMapper = new Application_Model_HotelMapper();
        
        //retrieves the user ID who signed-in
        //$username = Zend_Auth::getInstance()->getIdentity();
        $id =$this->getRequest()->getParam('id');
        $user = $userMapper->findUserById($id);
        $checkedHotels = $userMapper->findHotelsByUser($id);
        $allHotels= $hotelMapper->fetchAll();
        $checklist= array();
        
        foreach ($allHotels as $h) {
            $checklist[$h->getId()]="unchecked";
        }
        
        foreach ($checkedHotels as $h) {
            $checklist[$h->getId()]="checked";
        }
        
        $this->view->user= $user;
        $this->view->entries=$allHotels;
        $this->view->checklist=$checklist;
    }

    public function saveAction() {
        $checklist= $this->getRequest()->getPost()['check_list'];
        //unset($checklist['save']);
        $userid = $this->getRequest()->getParam('id');
        $ownershipMapper = new Application_Model_UsersHotelsMapper();
        
        //TODO: make it a transaction
        $ownershipMapper->deleteByUserId($userid);

        for ($i = 0; $i < count($checklist); ++$i) {
           $ownershipMapper->add($userid ,$checklist[$i] );
        }
    }
}



