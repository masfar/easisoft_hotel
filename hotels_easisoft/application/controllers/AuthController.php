<?php

class AuthController extends Zend_Controller_Action
{

    public function loginAction() {
        
        $db = $this->_getParam('db');
        $loginForm = new Application_Form_Login();
        
        if ($loginForm->isValid($_POST)) {
            
            $adapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');
            // 'MD5(CONCAT(?, password_salt))'
            $adapter->setIdentity($loginForm->getValue('username'));
            $adapter->setCredential($loginForm->getValue('password'));
            
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($adapter);
            
            if ($result->isValid()) {
                // $userMapper = new Application_Model_UserMapper();
                // $auth->getStorage()->write($userMapper->findUserBy($username));
                
                //$user_session->user_id = 3;
                $this->_helper->FlashMessenger('Successful Login');
                $this->_redirect('/hotels');
                return;
            }
        }
        
        $this->view->loginForm = $loginForm;
    }

    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_redirect('/');
    }
}





