<?php

class Application_Model_UserMapper
{

    protected $_dbTable;
    
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Users');
        }
        return $this->_dbTable;
    }
    
    public function findHotelsByUser($id) {
    
        $result = $this->getDbTable()->find($id);
    
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
    
        $resultSet = $row->findManyToManyRowset(
            'Application_Model_DbTable_Hotels',
            'Application_Model_DbTable_UsersHotels' );
        //var_dump($row);
    
        $entries = array();
    
        foreach ($resultSet as $row) {
            $hotel = Application_Model_HotelMapper::buildHotel($row);
            $entries[] = $hotel;
            //var_dump($priceList);
             
        }
        return $entries;
    }
    
    public function findUserBy($name) {
    
        $row = $this->getDbTable()->fetchRow(
            $this->getDbTable()->select()
            ->where('username = :name')
            ->bind(array(':name'=> $name))
        );
    
        return $this->buildUser($row);
         
   }
   
   public function findUserById($id) 
   {
      $row = $this->getDbTable()->find($id);
       return $this->buildUser($row->current());
   }
    
    private function buildUser($row)
    {
        $user = new Application_Model_User();
        $user->setId($row->id)
           ->setUsername($row->username)
           ->setEmail($row->email)
           ->setRole($row->role)
           ->setHotels($this->findHotelsByUser(($row->id)));
        return $user;
    }

    function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        return $this->mapUserList($resultSet);

    }

    function mapUserList($resultSet)
    {
        $entries = array();
        foreach ($resultSet as $row) {
            $user = $this->buildUser($row);
            $entries[] = $user;
        }
        return $entries;
    }
    
    public function save(Application_Model_User $user)
    {
        $data = array(
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'password' => $user->getPassword(),
            'email' => $user->getEmail(),
            'role' => $user->getRole(),
        );

        if ('' === ($id = $user->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array(
                'id = ?' => $id
            ));
    
        }
    }
    
    public function delete($id){
        $usershotels = new Application_Model_UsersHotelsMapper();
        $usershotels->deleteByUserId($id);
        $row = $this->getDbTable()->delete("id=".$id);
        
    }
          
    
}

