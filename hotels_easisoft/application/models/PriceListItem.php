<?php

class Application_Model_PriceListItem
{
    protected $_desc;
    protected $_price;
    protected $_id;
    
    public function setDesc($text) {
        $this->_desc = (string) $text;
        return $this;
    }
    
    public function getDesc() {
        return $this->_desc;
    }
    
    public function setPrice($price) {
        $this->_price = (double) $price;
        return $this;
    }
    
    public function getPrice() {
        return $this->_price;
    }
    
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getId() {
        return $this->_id;
    }

}

