<?php

class Application_Model_HotelMapper
{

    protected $_dbTable;
    protected $_userHoteldbTable;
    protected $_priceListdbTable;
    
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Hotels');
        }
        return $this->_dbTable;
    }
    
    
    public function save(Application_Model_Hotel $hotel)
    {

        $data = array(
            'id' => $hotel->getId(),
            'name' => $hotel->getName(),
            'address' => $hotel->getAddress(),
            'stars' => $hotel->getStars()
        );
        
        if (0 === ($id = $hotel->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
           
        } else {
            $this->getDbTable()->update($data, array(
                'id = ?' => $id
            )); 
            
        }
    }
    
        
    public function findHotel($id)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        
        foreach ($result as $row) {
               $hotel = $this->buildHotel($row);
        }
        
        return $hotel;
    }
    
    public static function buildHotel($row)
    {
        $hotel = new Application_Model_Hotel();
        $hotel->setId($row->id)
            ->setName($row->name)
            ->setAddress($row->address)
            ->setStars($row->stars);
        
        $priceList = $row->findDependentRowset('Application_Model_DbTable_PriceList');
        foreach ($priceList as $priceItem) {
            $priceEntry = new Application_Model_PriceListItem();
            $priceEntry->setDesc($priceItem->desc);
            $priceEntry->setPrice($priceItem->price);
            $priceEntry->setId($priceItem->id);
            $hotel->addPriceItem($priceEntry);
        }

        return $hotel;
    }
    
    function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        return $this->mapHotelList($resultSet);

    }

    function mapHotelList($resultSet)
    {
        $entries = array();
        foreach ($resultSet as $row) {
            $hotel = $this->buildHotel($row);
            $entries[] = $hotel;
           //echo var_dump($row);
        }
        return $entries;
    }
    
  
    public function delete($id){
        $this->getDbTable()->delete("id=".$id);
        //TODO: delete pricelist for hotel 
    }
    

}


