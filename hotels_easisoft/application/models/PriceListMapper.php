<?php

class Application_Model_PriceListMapper
{

    protected $_dbTable;
    
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    
    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_PriceList');
        }
        return $this->_dbTable;
    }
    
    public function delete($id){
        
        $this->getDbTable()->delete("id=".$id);
    }
    
    public function add($hotel_id, $desc ,$price){
       
        $data = array('hotel_id' => $hotel_id, 'desc' => $desc, 'price' => $price);
        $this->getDbTable()->insert($data);
    }

    public function update($priceItemid, $desc ,$price){
         
        $data = array('desc' => $desc, 'price' => $price);
        $this->getDbTable()->update($data, 'id='.$priceItemid);
    }
    
}

