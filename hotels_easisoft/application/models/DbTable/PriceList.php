<?php

class Application_Model_DbTable_PriceList extends Zend_Db_Table_Abstract
{

    protected $_name = 'pricelist';
    protected $_referenceMap    = array(
        'Application_Model_DbTable_Hotels' => array(
            'columns'           => array('hotel_id'),
            'refTableClass'     => 'Application_Model_DbTable_Hotels',
            'refColumns'        => array('id')
        )
    );

}

