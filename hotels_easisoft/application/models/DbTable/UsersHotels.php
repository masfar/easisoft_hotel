<?php

class Application_Model_DbTable_UsersHotels extends Zend_Db_Table_Abstract
{

    protected $_name = 'UsersHotels';
    
    protected $_referenceMap    = array(
        'Application_Model_DbTable_Users' => array(
            'columns'           => array('user_id'),
            'refTableClass'     => 'Application_Model_DbTable_Users',
            'refColumns'        => array('id')
        ),
        'Application_Model_DbTable_Hotels' => array(
            'columns'           => array('hotel_id'),
            'refTableClass'     => 'Application_Model_DbTable_Hotels',
            'refColumns'        => array('id')
        )
    );


}

?>

