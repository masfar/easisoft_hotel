<?php

class Application_Model_Hotel
{

    protected $_name;

    protected $_address;

    protected $_stars;
    // protected $_users;
    protected $_id;
    
    protected $_priceList=array();

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid hotel property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid hotel property');
        }
        return $this->$method();
    }

    public function setOptions($options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setName($text)
    {
        $this->_name = (string) $text;
        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setAddress($text)
    {
        $this->_address = (string) $text;
        return $this;
    }

    public function getAddress()
    {
        return $this->_address;
    }

    public function setStars($text)
    {
        $this->_stars = (string) $text;
        return $this;
    }

    public function getStars()
    {
        return $this->_stars;
    }
    
    // public function setUsers(){}
    // public function getUsers{}
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }
    
    public function getPriceList() {
      return $this->_priceList;  
    }
    
    public function addPriceItem($priceItem) {
        $this->_priceList[]=$priceItem;
    }
}


