<?php

class Application_Model_UsersHotelsMapper
{

    protected $_dbTable;
    
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    
    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_UsersHotels');
        }
        return $this->_dbTable;
    }
    
    public function deleteByUserId($id){
        
        $this->getDbTable()->delete("user_id=".$id);
        
    
    }
    
    public function add($id ,$hotelId ){
       
        $data = array('user_id' => $id, 'hotel_id' => $hotelId);
        $this->getDbTable()->insert($data);
        
    
    }

}

