<?php

 class Application_Model_User implements Zend_Acl_Role_Interface
{

    protected $_id;
    protected $_username;
    protected $_password;
    protected $_email;
    protected $_role;
    protected $_hotels;
    
    protected $_aclRoleId = null;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        $this->$method($value);
    }
    
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        return $this->$method();
    }
    
    public function setOptions($options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }
    
    public function getId() {
        return $this->_id;
    }
    
    public function setUsername($username) {
        $this->_username = $username;
        return $this;
    }
    
    public function getUsername() {
        return $this->_username;
    }
    
    public function setPassword($password) {
        $this->_password = $password;
        return $this;
    }
    
    public function getPassword() {
        return $this->_password;
    }
    
    public function setEmail($email) {
        $this->_email = $email;
        return $this;
    }
    
    public function getEmail() {
        return $this->_email;
    }
    
    public function setRole($role) {
        $this->_role = $role;
        return $this;        
    }
    
    public function getRole() {
        return $this->_role;
    }
    
    public function getRoleId()
    {
        if ($this->_aclRoleId == null) {
            return 'regular';
        }
        
        return $this->_aclRoleId;
    }
    
    public function setHotels($hotels) {
        $this->_hotels = $hotels;
        return $this;
    }
    
    public function getHotels() {
        return $this->_hotels;
    }
    
    
    
} 
/*
class Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';
    protected $_primary = 'username';
}
*/
